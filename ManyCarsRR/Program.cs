﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyCarsRR
{
    class Car
    {
        internal int Id;
        internal string Plate;
        internal string Model;
        internal int Yop;

        internal Car(int id, string plate, string model, int yop)
        {
            Id = id;
            Plate = plate;
            Model = model;
            Yop = yop;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, Car> carsById = new Dictionary<int, Car>();
            Car car1 = new Car(2, "FFF34", "BMW", 2010);
            Car car2 = new Car(4, "IOP234", "Audi", 2003);
            Car car3 = new Car(5, "POI987", "Toyota", 2015);
            Car car4 = new Car(7, "123GTB", "Nissan", 2008);
            Car car5 = new Car(8, "689IUJM", "Hyundai", 2006);

            carsById.Add(car1.Id, car1);
            carsById.Add(car2.Id, car2);
            carsById.Add(car3.Id, car3);
            carsById.Add(car4.Id, car4);
            carsById.Add(car5.Id, car5);

            Car foundById = carsById[7];
            Console.WriteLine("Found car key = 7: {0}", foundById.Plate);
            Console.ReadKey();
        }
    }
}
