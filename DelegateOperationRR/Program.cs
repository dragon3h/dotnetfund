﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

delegate double AnyOperationDelegate(double a, double b);
namespace DelegateOperationRR
{
    class Program
    {
        public static double add(double a, double b)
        {
            return a + b;
        }

        public static double sub(double a, double b)
        {
            return a - b;
        }

        public static double mul(double a, double b)
        {
            return a * b;
        }

        public static double div(double a, double b)
        {
            return a / b;
        }
        static void Main(string[] args)
        {
            AnyOperationDelegate currentOperation = null;
            int numOfOperation;
            int a;
            int b;

            try
            {
                Console.WriteLine("Enter the number of operation (1 - add/ 2 - sub/ 3 -mul/ 4 - div): ");
                int.TryParse(Console.ReadLine(), out numOfOperation);
                Console.WriteLine("Enter the number a: ");
                int.TryParse(Console.ReadLine(), out a);
                Console.WriteLine("Enter the number b: ");
                int.TryParse(Console.ReadLine(), out b);

                switch (numOfOperation)
                {
                    case 1:
                        currentOperation = new AnyOperationDelegate(add);
                        break;
                    case 2:
                        currentOperation = new AnyOperationDelegate(sub);
                        break;
                    case 3:
                        currentOperation = new AnyOperationDelegate(mul);
                        break;
                    case 4:
                        currentOperation = new AnyOperationDelegate(div);
                        
                        break;
                }
                Console.WriteLine("Result is: " + currentOperation(a, b));
            }
            catch (IOException e)
            {
                Console.WriteLine("Error input.");
                Console.WriteLine(e.StackTrace);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception happened: {0}", e.Message);
                Console.WriteLine(e.StackTrace);
            }
            
            Console.ReadKey();
        }
    }
}
