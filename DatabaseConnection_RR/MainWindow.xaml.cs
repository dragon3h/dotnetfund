﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DatabaseConnection_RR
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            DatabaseConnection_RR.Northwind2014DataSet northwind2014DataSet = ((DatabaseConnection_RR.Northwind2014DataSet)(this.FindResource("northwind2014DataSet")));
            // Load data into the table Customers. You can modify this code as needed.
            DatabaseConnection_RR.Northwind2014DataSetTableAdapters.CustomersTableAdapter northwind2014DataSetCustomersTableAdapter = new DatabaseConnection_RR.Northwind2014DataSetTableAdapters.CustomersTableAdapter();
            northwind2014DataSetCustomersTableAdapter.Fill(northwind2014DataSet.Customers);
            System.Windows.Data.CollectionViewSource customersViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("customersViewSource")));
            customersViewSource.View.MoveCurrentToFirst();
            // Load data into the table Categories. You can modify this code as needed.
            DatabaseConnection_RR.Northwind2014DataSetTableAdapters.CategoriesTableAdapter northwind2014DataSetCategoriesTableAdapter = new DatabaseConnection_RR.Northwind2014DataSetTableAdapters.CategoriesTableAdapter();
            northwind2014DataSetCategoriesTableAdapter.Fill(northwind2014DataSet.Categories);
            System.Windows.Data.CollectionViewSource categoriesViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("categoriesViewSource")));
            categoriesViewSource.View.MoveCurrentToFirst();
            // Load data into the table Products. You can modify this code as needed.
            DatabaseConnection_RR.Northwind2014DataSetTableAdapters.ProductsTableAdapter northwind2014DataSetProductsTableAdapter = new DatabaseConnection_RR.Northwind2014DataSetTableAdapters.ProductsTableAdapter();
            northwind2014DataSetProductsTableAdapter.Fill(northwind2014DataSet.Products);
            System.Windows.Data.CollectionViewSource categoriesProductsViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("categoriesProductsViewSource")));
            categoriesProductsViewSource.View.MoveCurrentToFirst();
        }
    }
}
