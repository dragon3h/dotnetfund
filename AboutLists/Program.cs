﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AboutLists
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] intArray = { 3, 5, 6 };
            // rectangular array - faster - one dereference to access
            int[,] int2DArray = { { 2, 3 }, { 1, 2 }, { 3, 4 } };
            // Java-style jagged array - two dereferences to access
            int[][] int2DArrayJagged;
            
            for (int i = 0; i < intArray.Length; i++)
            {
                Console.Write(intArray[i] + ", ");

            }
            Console.WriteLine();
            foreach (int val in intArray)
            {
                Console.WriteLine(val + ", ");
            }

            Console.WriteLine(int2DArray.GetLength);
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 2; col++)
                {
                    Console.Write(int2DArray[row, col] + " ");
                }
                Console.WriteLine();
            }

                Console.ReadKey();
        }
    }
}
