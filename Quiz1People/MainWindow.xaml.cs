﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz1People
{
    class Person
    {
        public Person(string firstName, string lastName, string photoPath)
        {
            FirstName = firstName;
            LastName = lastName;
            PhotoPath = photoPath;
        }

        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string PhotoPath { set; get; }
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<Person> PersonList = new ObservableCollection<Person>();
        string filePath = null;
        string imgPath = null;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenClick(object sender, RoutedEventArgs e)
        {
            PersonList.Clear();

            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "People's list (*.people)|*.people*|All files (*.*)|*.*";
            openFile.FilterIndex = 1;
            openFile.Multiselect = false;

            try
            {
                if (openFile.ShowDialog() == true)
                {
                    filePath = openFile.FileName;
                    string[] lines = File.ReadAllLines(filePath);
                    foreach (string line in lines)
                    {

                        string[] personData = line.Split(';');
                        string firstName = personData[0];
                        string lastName = personData[1];
                        string photoPath = personData[2];

                        Person p = new Person(firstName, lastName, photoPath);
                        PersonList.Add(p);
                    }
                    lvPeopleList.ItemsSource = PersonList;
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("Read file exception occured!");
                MessageBox.Show(ex.Message);
            }

        }

        private void SaveAsClick(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.Filter = "Person's list (*.people)|*.people*|All files (*.*)|*.*";

            try
            {
                if (saveFile.ShowDialog() == true)
                {
                    string[] listOfPerson = new string[PersonList.Count];
                    int ind = 0;
                    foreach (Person p in PersonList)
                    {
                        string firstName = p.FirstName;
                        string lastName = p.LastName;
                        string photoPath = p.PhotoPath;
                        string personLine = firstName + ";" + lastName + ";" + photoPath;
                        listOfPerson[ind] = personLine;
                        ind++;
                    }
                    File.WriteAllLines(saveFile.FileName, listOfPerson);

                }
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ExitClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvPeopleList.SelectedIndex != -1)
            {
                int index = lvPeopleList.SelectedIndex;
                showPerson(PersonList[index]);
            }
        }

        private void btPickPicture_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Title = "Select a picture";
            open.Filter = "Image file (*.jpg,*.jpeg,*.png,*.gif)|*.jpg;*.jpeg;*.png;*.gif|All Files (*.*)|*.*";


            open.FilterIndex = 1;
            open.Multiselect = false;
            try
            {
                if (open.ShowDialog() == true)
                {
                    imgPath = open.FileName;
                    imgPreview.Source = new BitmapImage(new Uri(imgPath));

                }
            }
            catch(IOException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (ArgumentNullException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void showPerson(Person person)
        {
            try
            {
                tbFirstName.Text = person.FirstName;
                tbLastName.Text = person.LastName;
                imgPath = person.PhotoPath;
                imgPreview.Source = new BitmapImage(new Uri(imgPath));
            }
            catch (UriFormatException ex)
            {
                MessageBox.Show("Format Uri exception occured!");
                MessageBox.Show(ex.Message);
            }
            catch (ArgumentNullException ex)
            {
                MessageBox.Show("File exception occured!");
                MessageBox.Show(ex.Message);
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
                string firstName = tbFirstName.Text;
                string lastName = tbLastName.Text;
                string photoPath = imgPath;
                PersonList.Add(new Person(firstName, lastName, photoPath));

                lvPeopleList.ItemsSource = PersonList;

        }

    }
}
