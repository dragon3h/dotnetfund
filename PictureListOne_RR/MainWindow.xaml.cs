﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PictureListOne_RR
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        
        
        private void btPickPicture_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Title = "Select a picture";
            open.Filter = "All supported grahhics|*.jpg;*.jpeg;*.png|" +
                "JPEG (*jpg;*.jpeg)|*.jpg;*.jpeg|" +
                "Portable Network Graphic (*.png)|*.png";
            if (open.ShowDialog() == true)
            {
                ImgPreview.Source = new BitmapImage(new Uri(open.FileName));
            }
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            Image img = new Image();
            img.Source = ImgPreview.Source;
            listBoxPictures.Items.Add(img);
            img.Width = 250;
            img.Height = 125;
        }

    }
}
