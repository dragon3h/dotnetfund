﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiveAndTake
{
    class Account
    {
        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                if (value.Length > 1)
                {
                    name = value;
                }
                else
                    {
                        throw new ArgumentException("Name has to have minimum 2 characters.");
                    }
                
            }
        }
            
        private double balance;
        public double Balance
        {
            get
            {
                return balance;
            }
            set
            {
                if (value >= 0)
                {
                    balance = value;
                }
            }
        }

        public Account(string name, double balance)
        {
            Name = name;
            Balance = balance;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            //try
            //{
            //    Account a = new Account("O", 222);
            //    a.Name = "One";
            //    a.Balance = 222;
            //    Account b = new Account("Two", 333);
            //    b.Name = "Two";
            //    b.Balance = -333;

            //    List<Account> accountList = new List<Account>();
            //    accountList.Add(a);
            //    accountList.Add(b);

            //    foreach (Account account in accountList)
            //    {
            //        Console.WriteLine("Account {0} has balance {1}", account.Name, account.Balance);
            //    }
            //}
            //catch (ArgumentException e)
            //{
            //    Console.WriteLine("Exception occured");
            //    //Console.WriteLine(e.Message);
            //}

            
            string fileName = "test.txt";
            string textToAdd = "Example text in file sldkfjasl;kdfj;alksjfkl;";
            FileStream fs = File.Create(fileName);

            try
            {
                StreamWriter writer = new StreamWriter(fileName, true);
                {
                    writer.Write(textToAdd);
                }

                string line = "";
                StreamReader sr = new StreamReader(fileName);
                {
                    while ((line = sr.ReadLine()) != null)
                    {
                        Console.WriteLine(line);
                    }
                }
            } catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
            

            Console.ReadKey();

            
            //Console.ReadKey();
        }
        
    }
}
