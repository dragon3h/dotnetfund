﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MessageBoxTest_RR
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btResponce_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Hello World!?", "Message Box", MessageBoxButton.YesNoCancel);
            switch(result)
            {
                case MessageBoxResult.Yes:
                    MessageBox.Show("Hello to you too!");
                    break;
                case MessageBoxResult.No:
                    MessageBox.Show("how you want(");
                    break;
                case MessageBoxResult.Cancel:
                    MessageBox.Show("Cansel - ok!");
                    break;
            }
        }

        private void btIcon_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Hello to you!", "Message Box", MessageBoxButton.OK, MessageBoxImage.Information);

        }
    }
}
