namespace School_EF_RR
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    public class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public List<Student> getAllStudents()
        {
            List<Student> resultList = new List<Student>();

            using (Model1 context = new Model1())
            {
                var rows = (from s in context.Students select s);
                foreach (Student std in rows)
                {
                    resultList.Add(std);
                }
            }
            return resultList;
        }

        public DbSet<Student> Students { get; set; }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}