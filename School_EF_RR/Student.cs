﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School_EF_RR
{
    public class Student
    {
        public Student()
        {

        }
        public Student(string firstName, string lastName, int age, double gpa)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
            GPA = gpa;
        }

        [Key]
        public int ID { get; set; }
        [Column("FirstName", TypeName = "nvarchar")]
        [MaxLength(50), MinLength(2)]
        [Required]
        public string FirstName { get; set; }
        //[Column("LastName", TypeName = "nvarchar")]
        [MaxLength(50), MinLength(2)]
        [Required]
        public string LastName { get; set; }
        [Column("Age", TypeName = "int")]
        public int Age { get; set; }
        [Column("GPA", TypeName = "float")]
        public double GPA { get; set; }
    }
}
