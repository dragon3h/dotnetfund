﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace School_EF_RR
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            // here you take control
            //MessageBox.Show("test");
            //try
            //{
            //    using (var ctx = new Model1())
            //    {

            //    }
            //}
            //catch (SqlException ex)
            //{
            //    Console.Write(ex.StackTrace);
            //    // TODO: message box
            //    System.Environment.Exit(1);
            //}
            //catch (EntityException ex)
            //{
            //    Console.Write(ex.StackTrace);
            //    // TODO: message box
            //    System.Environment.Exit(1);
            //}
        }

        //public List<Student> getAllStudents()
        //{
        //    List<Student> resultList = new List<Student>();
        //    using (var con = new    EntityConnection("Model1"))
        //    {
        //        con.Open();
        //        EntityCommand cmd = con.CreateCommand();
        //        cmd.CommandText = "SELECT * FROM Students";
        //        Dictionary<int, string> dict = new Dictionary<int, string>();
        //        using (EntityDataReader rdr = cmd.ExecuteReader(CommandBehavior.SequentialAccess | CommandBehavior.CloseConnection))
        //        {
        //            while (rdr.Read())
        //            {
        //                Student std = new Student();
        //                int ID = rdr.GetInt32(0);
        //                string firstName = rdr.GetString(1);
        //                string lastName = rdr.GetString(2);
        //                int age = rdr.GetInt32(3);
        //                double gpa = rdr.GetDouble(4);
        //                std.ID = ID;
        //                std = new Student(firstName, lastName, age, gpa);

        //                resultList.Add(std);
        //            }
        //        }
        //    }
        //    return resultList;
        //}

        //private void databaseConnect()
        //{
        //    using (var ctx = new Model1())
        //    {
        //        Student stud = new Student() { FirstName = "Semen", LastName = "Semenov", Age = 25, GPA = 3.8 };

        //        ctx.Students.Add(stud);
        //        ctx.SaveChanges();
        //    }
        //}

        //public List<Student> getAllStudents()
        //{
        //    List<Student> resultList = new List<Student>();
        //    using (SqlCommand cmd = new SqlCommand("SELECT ID, Name, Age, GPA FROM Students", Connection))
        //    {
        //        using (SqlDataReader reader = cmd.ExecuteReader())
        //        {
        //            while (reader.Read())
        //            {
        //                int ID = reader.GetInt32(0);
        //                string name = reader.GetString(1);
        //                int age = reader.GetInt32(2);
        //                double gpa = reader.GetDouble(3);
        //                resultList.Add(new Student(ID, name, age, gpa));
        //            }
        //        }
        //    }
        //    return resultList;
        //}

        //public Student findHighestGPAStudent()
        //{
        //    Student bestStudent = null;
        //    using (SqlCommand cmd = new SqlCommand("SELECT * FROM Students WHERE GPA = (SELECT MAX(GPA) FROM Students)", Connection))
        //    {
        //        using (SqlDataReader reader = cmd.ExecuteReader())
        //        {
        //            while (reader.Read())
        //            {
        //                int ID = reader.GetInt32(0);
        //                string name = reader.GetString(1);
        //                int age = reader.GetInt32(2);
        //                double gpa = reader.GetDouble(3);
        //                bestStudent = new Student(ID, name, age, gpa);
        //            }
        //        }
        //    }
        //    return bestStudent;
        //}
    }
}
