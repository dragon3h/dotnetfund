﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Core;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace School_EF_RR
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            grdStudents.ItemsSource = getAllStudents();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {

            using (var ctx = new Model1())
            {
                string firstName = tbName.Text;
                string lastName = tbLastName.Text;
                int age = Int32.Parse(tbAge.Text);
                double gpa = Double.Parse(tbGPA.Text);
                Student std = new Student(firstName, lastName, age, gpa);

                ctx.Students.Add(std);
                ctx.SaveChanges();
                grdStudents.ItemsSource = getAllStudents();
            }
        }

        public List<Student> getAllStudents()
        {

            List<Student> resultList = new List<Student>();

            using (Model1 context = new Model1())
            {
                var rows = (from s in context.Students select s);
                foreach (Student std in rows) {
                    resultList.Add(std);
                }
            }
            return resultList;
        }
    }
}
