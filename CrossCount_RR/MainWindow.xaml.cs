﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CrossCount_RR
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    class WordsComparatorByOccures : Comparer<int>
    {
        public override int Compare(int a, int b)
        {
            if (a.CompareTo(b) != 0)
            {
                return b.CompareTo(a);
            }
            else
                return 0;
        }
    }

    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DateTime dt = DateTime.Now;
            DateTime dateValue = DateTime.Parse("4/3/2016", CultureInfo.InvariantCulture);
            Console.WriteLine((int)dateValue.DayOfWeek);

            lblResult.Content = "";
            Dictionary<string, int> text1 = new Dictionary<string, int>();
            Dictionary<string, int> text2 = new Dictionary<string, int>();
            Dictionary<string, int> result = new Dictionary<string, int>();
            Dictionary<string, int> resultOut = new Dictionary<string, int>();
            string str1 = tboxText1.Text;
            string str2 = tboxText2.Text;
            string[] arrayOfWords1 = System.Text.RegularExpressions.Regex.Split(str1, "[^\\w]");
            string[] arrayOfWords2 = System.Text.RegularExpressions.Regex.Split(str2, "[^\\w]");

            foreach (string word in arrayOfWords1)
            {
                if (!text1.ContainsKey(word))
                {
                    text1.Add(word, 0);
                }
                text1[word]++;
            }
            foreach (string word in arrayOfWords2)
            {
                if (!text2.ContainsKey(word))
                {
                    text2.Add(word, 0);
                }
                text2[word]++;
            }
            foreach (string word in text2.Keys)
            {
                int temp = 0;
                if (text1.ContainsKey(word))
                {
                    temp = text1[word] + text2[word];
                    result.Add(word, temp);
                }
            }
            resultOut = result;
            int max = 0;
            string keyMax = "";

            while (resultOut.Count > 0)
            { 
                max = 0;
                keyMax = "";
                foreach (string w in resultOut.Keys)
                {
                    if (max < resultOut[w])
                    {
                        max = resultOut[w];
                        keyMax = w;
                    }
                }
                lblResult.Content += string.Format(keyMax + " : {0}\n", resultOut[keyMax]);
                resultOut.Remove(keyMax);
            }

            foreach (string w in result.Keys)
            {
                lblResult.Content += string.Format(w + " : {0}\n", result[w]);
            }
            
        }
    }
}
