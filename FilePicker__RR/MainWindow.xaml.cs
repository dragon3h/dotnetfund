﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilePicker__RR
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<string> listOfPictures = new List<string>();
        string filePath = null;
        string fileSavePath = null;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenClick(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Image list (.piclist)|*.piclist|All Files (*.*)|*.*";


                openFileDialog.FilterIndex = 1;
                openFileDialog.Multiselect = false;

                if (openFileDialog.ShowDialog() == true)
                {
                    fileSavePath = openFileDialog.FileName;
                    string[] lines = File.ReadAllLines(openFileDialog.FileName);
                    listBox.Items.Clear();

                    foreach (string path in lines)
                    {
                        Image img = new Image();
                        img.Width = 140;
                        img.Source = new BitmapImage(new Uri(path));
                        listBox.Items.Add(img);
                        listOfPictures.Add(path);
                    }
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("Exception occured!");
                MessageBox.Show(ex.Message);

            }
        }

        private void SaveAsClick(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Image list (.piclist)|*.piclist|All Files (*.*)|*.*";

            if (saveFileDialog.ShowDialog() == true)
            {
                    File.WriteAllLines(saveFileDialog.FileName, listOfPictures);
            }
        }

        private void ExitClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btPickPicture_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Title = "Select a picture";
            open.Filter = "Any image file (*.jpg,*.jpeg,*.png,*.gif)|*.jpg;*.jpeg;*.png;*.gif|All Files (*.*)|*.*";

            open.FilterIndex = 1;
            open.Multiselect = false;

            if (open.ShowDialog() == true)
            {
                filePath = open.FileName;
                imgPreview.Source = new BitmapImage(new Uri(filePath));
            }
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            if (filePath == null)
            {
                return;
            }
            Image img = new Image();
            img.Source = imgPreview.Source;
            listBox.Items.Add(img);
            img.Width = 140;
            listOfPictures.Add(filePath);
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int index = listBox.Items.IndexOf(listBox.SelectedItem);
                if (index > -1)
                {
                    listBox.Items.RemoveAt(index);
                    listOfPictures.RemoveAt(index);
                    File.WriteAllLines(fileSavePath, listOfPictures);
                }
                else
                {
                    MessageBox.Show("You chose no picture!");
                }
                
            }
            catch (IOException ex)
            {
                MessageBox.Show("Delete exception occured!");
                MessageBox.Show(ex.Message);
            }
            
        }
    }
}
