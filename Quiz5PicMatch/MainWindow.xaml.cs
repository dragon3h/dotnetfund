﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz5PicMatch
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<string> listOfPictures = new List<string>();
        string filePath = null;
        int indexList = -1;
        int indexRandom;
        string[] lines;
        string[] linesRandom;
        public MainWindow()
        {
            InitializeComponent();
            //((INotifyCollectionChanged)lbList.Items).CollectionChanged += mListBox_CollectionChanged;  
        }

        //private void mListBox_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        //{
        //    if (e.Action == NotifyCollectionChangedAction.Add)
        //    {
        //        // scroll the new item into view   
        //        lbList.ScrollIntoView(e.NewItems[0]);
        //    }
        //}

        //EventManager.RegisterClassHandler(typeof(ListBoxItem), ListBoxItem.MouseLeftButtonDownEvent, 
        //    new RoutedEventHandler(this.MouseLeftButtonDownClassHandler));
        //private void OnMouseLeftButtonDown(object sender, RoutedEventArgs e)
        //{
        //}

        private void ExitClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void LoadClick(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Image list (.piclist)|*.piclist|All Files (*.*)|*.*";


                openFileDialog.FilterIndex = 1;
                openFileDialog.Multiselect = false;

                if (openFileDialog.ShowDialog() == true)
                {
                    filePath = openFileDialog.FileName;
                    lines = File.ReadAllLines(openFileDialog.FileName);
                    lbList.Items.Clear();

                    linesRandom = new string[lines.Length];
                    
                    List<int> randomList = new List<int>();
                    int num = 0;

                    Random rnd = new Random();
                    int count = 0;
                    while (count < lines.Length)
                    {
                        num = rnd.Next(0, lines.Length);
                        if (!randomList.Contains(num))
                        {
                            randomList.Add(num);
                            linesRandom[count] = lines[num];
                            count++;
                        } 
                    }

                    foreach (string path in lines)
                    {
                        Image img = new Image();
                        img.Width = 135;
                        img.Source = new BitmapImage(new Uri(path));
                        lbList.Items.Add(img);
                        listOfPictures.Add(path);
                    }

                    foreach (string path in linesRandom)
                    {
                        Image img = new Image();
                        img.Width = 135;
                        img.Source = new BitmapImage(new Uri(path));
                        lbRandom.Items.Add(img);
                        //listOfPictures.Add(path);
                    }
                    //string str = lbList.SelectedItem.ToString();
                    //MessageBox.Show(str);

                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("Exception occured!");
                MessageBox.Show(ex.Message);
            }
        }


        private void lbList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            indexList = lbList.Items.IndexOf(lbList.SelectedItem);
            string str = lbList.SelectedItem.ToString();
            MessageBox.Show(" the selected path is: " + str);

        }

        private void lbRandom_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (indexList == -1)
            {
                return;
            }
            indexRandom = lbRandom.Items.IndexOf(lbRandom.SelectedItem);
            if (lines[indexList] == linesRandom[indexRandom])
            {
                Image img = new Image();
                img.Width = 135;
                string path = lines[indexList];
                img.Source = new BitmapImage(new Uri(path));
                lbMatches.Items.Add(img);
                lbList.SelectedValuePath = null;
            }
            
        }

        private void lbList_MouseDown(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("click!!!");
        }
    }
}
