﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("What is your name?");
            string name = Console.ReadLine();
            Console.Write("What is your age?");
            string ageStr = Console.ReadLine();
            int age;
            if (int.TryParse(ageStr, out age)) {
//            Console.WriteLine("Nice to meet you, " + name + "!");
            string greeting = string.Format("Nice to meet you, {0}! You are {1} years old, {0}.", name, age);
            Console.WriteLine(greeting);
            }
            else
            {
                Console.WriteLine("Age was invalid.");
            }
            Console.ReadKey();
        }
    }
}
