﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_3_DB
{
    class Person
    {
        public Person(int id, string name, int age)
        {
            ID = id;
            Name = name;
            Age = age;
        }
        public int ID { set; get; }
        public string Name { set; get; }
        public int Age { set; get; }
    }
}
