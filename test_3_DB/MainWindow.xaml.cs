﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace test_3_DB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Person> listOfPerson = new List<Person>();
        int ID = 0;
        static SqlConnection Connection = new SqlConnection(@"Data Source=p6rbnv719l.database.windows.net;Initial Catalog=calendar2;Integrated Security=False;User ID=ipd7abbott;Password=JohnAbbott2000;Connect Timeout=60;Encrypt=False;TrustServerCertificate=False");
        //static SqlConnection Connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\IPD\DotNet\Code_DotNet\test_3_DB\People.mdf;Integrated Security=True;Connect Timeout=30");
        //static SqlConnection Connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=H:\test_3_DB\People.mdf;Integrated Security=True");
        Database db = new Database(Connection);
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            //PersonList.Clear();
            string name = tbName.Text;
            int age = Int32.Parse(tbAge.Text);
            ID++;
            Person person = new Person(ID, name, age);
            //listOfPerson.Add(person);
            db.addPerson(person);
            
            lvPerson.ItemsSource = db.getAllPerson();
        }

    }
}
