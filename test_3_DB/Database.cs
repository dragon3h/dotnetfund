﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_3_DB
{
    class Database
    {
        public Database(SqlConnection connection)
        {
            Connection = connection;
            Connection.Open();
        }

        private SqlConnection Connection;

        public void addPerson(Person p)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                string name = p.Name;
                int age = p.Age;
                cmd.CommandText = "INSERT INTO Person (name, age) VALUES (@Name, @Age)";
                cmd.Parameters.AddWithValue("@Name", name);
                cmd.Parameters.AddWithValue("@Age", age);

                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = Connection;
                cmd.ExecuteNonQuery();
            }
        }

        public List<Person> getAllPerson()
        {
            List<Person> resultList = new List<Person>();
            using (SqlCommand cmd = new SqlCommand("SELECT ID, Name, Age FROM Person", Connection))
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int ID = reader.GetInt32(0);
                        string name = reader.GetString(1);
                        int age = reader.GetInt32(2);
                        resultList.Add(new Person(ID, name, age));
                    }
                }
            }
            return resultList;
        }
    }
}
