﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2LibraryRR
{
    public class Book
    {
        private string title;
        public string Title
        {
            get
            {
                return title;
            }
        }
        private string author;
        public string Author
        {
            get
            {
                return author;
            }
        }
        private int yop; // year of publication
        public int YOP
        {
            get
            {
                return yop;
            }
        }
        private decimal price;
        public decimal Price
        {
            get
            {
                return price;
            }
        }

        public Book(string lineOfData)
        {
            string[] fieldArray = lineOfData.Split(';');

            title = fieldArray[0];
            if (title.Length < 2)
            {
                throw new ArgumentException(
                    string.Format("Invalid field value '{0}', " +
                    "title must be at least 2 characters long in {1}",
                    title, lineOfData)
                    );
            }

            author = fieldArray[1];
            if (author.Length < 2)
            {
                throw new ArgumentException(
                    string.Format("Invalid field value '{0}', " +
                    "author must be at least 2 characters long in {1}",
                    author, lineOfData)
                    );
            }

            if (!int.TryParse(fieldArray[2], out yop))
            {
                throw new ArgumentException(
                    string.Format("Invalid field value '{0}', " +
                    "must be a integer in {1}",
                    fieldArray[2], lineOfData)
                    );
            }
            if (yop < 1900 || yop > 2100)
            {
                throw new ArgumentException("Year of publication has to be between 1900 and 2100");
            }


            if (!decimal.TryParse(fieldArray[3], out price))
            {
                throw new ArgumentException(
                    string.Format("Invalid field value '{0}', " +
                    "must be a decimal in {1}",
                    fieldArray[3], lineOfData)
                    );
            }
            if (price < 0)
            {
                throw new ArgumentException("Price must not be negative in {0}", lineOfData);
            }
        }

    }
    class Program
    {
        static List<Book> library = new List<Book>();
        static void Main(string[] args)
        {
            decimal totalValue = 0;
            try
            {
                string[] linesArray =
                    File.ReadAllLines("library.txt");
                foreach (string line in linesArray)
                {
                    Book book = new Book(line);
                    library.Add(book);
                }

                foreach (Book book in library)
                {
                    Console.WriteLine("Title: " + book.Title + "; author: " + book.Author + "; year of publication: " + book.YOP
                        + "; price: " + book.Price);
                }

                List<Book> SortedList1 = library.OrderBy(o => o.YOP).ToList();

                Console.WriteLine("The oldest book is: " + SortedList1[0].Title + "; year of publication: " + SortedList1[0].YOP);

                List<Book> SortedList2 = library.OrderBy(o => o.Price).ToList();

                Console.WriteLine("The  most expensive book is: " + SortedList2[SortedList2.Count - 1].Title 
                    + "; Price is: " + SortedList2[SortedList2.Count - 1].Price);

                foreach (Book book in library)
                {
                    totalValue += book.Price;
                }
                Console.WriteLine("The total value of all books in the library is: " + totalValue);

            }
            catch (IOException e)
            {
                Console.WriteLine("Error accessing file.");
                Console.WriteLine(e.StackTrace);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception happened: {0}", e.Message);
                Console.WriteLine(e.StackTrace);
            }
            Console.ReadKey();
        }
    }
}
