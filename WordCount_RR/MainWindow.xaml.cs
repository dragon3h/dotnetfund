﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WordCount_RR
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, int> words = new Dictionary<string, int>();

            string theExclusions = tboxExclusions.Text;
            string[] splitExclusions = System.Text.RegularExpressions.Regex.Split(theExclusions, "[^\\w]");

            string theText = tboxText.Text;
            string[] splitWord = theText.Split(' ');

            foreach (string w in splitWord)
            {
                
                if (!words.ContainsKey(w))
                {
                    words.Add(w, 1);
                }
                else
                {
                    words[w]++;
                }
            }

            int max = 0;
            string word = "";
            foreach (string w in words.Keys)
            {
                if (splitExclusions.Contains(w))
                {
                    continue;
                }
                if (words[w] > max)
                {
                    max = words[w];
                    word = w;
                }
            }

            lblWord.Content = word;
            MessageBox.Show("The maximum word reapet: " + max);
        }
    }
}
