﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DababaseOne_RR
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        SqlConnection Connection;

        private void ButtonConnect_Click(object sender, RoutedEventArgs e)
        {
            Connection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\NewPeopleDB.mdf;Integrated Security=True;Connect Timeout=30");
            Connection.Open();
        }

        private void BattonAddData_Click(object sender, RoutedEventArgs e)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "INSERT INTO Person (Name, Age) VALUES (@Name, @Age)";
                cmd.Parameters.AddWithValue("@Name", "Jerry");
                cmd.Parameters.AddWithValue("@Age", 23);

                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = Connection;
                cmd.ExecuteNonQuery();
            }
            
        }

        private void ButtonFetchData_Click(object sender, RoutedEventArgs e)
        {
            using (SqlCommand cmd = new SqlCommand("SELECT * FROM Person", Connection))
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int ID = reader.GetInt32(0);
                        string name = reader.GetString(1);
                        int age = reader.GetInt32(2);
                        Console.WriteLine("Row {0}: {1} is {2} years old", ID, name, age);
                    }
                }
            }
        }
    }
}
