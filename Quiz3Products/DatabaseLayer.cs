namespace Quiz3Products
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    public class DatabaseLayer : DbContext
    {
        public DatabaseLayer()
            : base("name=DatabaseLayer")
        {
        }

        public DbSet<Product> Products { get; set; }


        public List<Product> getAllProducts()
        {
            List<Product> resultList = new List<Product>();

            using (DatabaseLayer context = new DatabaseLayer())
            {
                var rows = (from p in context.Products select p);
                foreach (Product prd in rows)
                {
                    resultList.Add(prd);
                }

            }
            return resultList;
        }

        public void addProduct(Product product)
        {
            using (DatabaseLayer context = new DatabaseLayer())
            {
                context.Products.Add(product);
                context.SaveChanges();
            }
        }

    }
}