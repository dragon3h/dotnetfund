﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz3Products
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BitmapImage image;
        int id = 0;
        public MainWindow()
        {
            InitializeComponent();
            using (var ctx = new DatabaseLayer())
            {
                grdProduct.ItemsSource = ctx.getAllProducts();
            }
        }
        private void grdProduct_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            Product prd = (Product)grdProduct.SelectedItem;
            if (prd == null)
            {
                return;
            }
            id = prd.ID;
            tbName.Text = prd.Name;
            tbPrice.Text = prd.Price.ToString();
            imgPreview.Source = ToImage(prd.Photo);
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            decimal price = decimal.Parse(tbPrice.Text);
            byte[] photo;
            if (!(image == null))
            {
                photo = getJPGFromImageControl(image as BitmapImage);
            }
            else
            {
                photo = null;
            }

            Product prd = new Product(name, price, photo);

            using (var ctx = new DatabaseLayer())
            {
                ctx.addProduct(prd);
                grdProduct.ItemsSource = ctx.getAllProducts();
                grdProduct.Items.Refresh();
            }
        }

        private void btSelectPhoto_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog open = new OpenFileDialog();
                open.Title = "Select a picture";
                open.Filter = "All supported grahhics|*.jpg;*.jpeg;*.png|" +
                    "JPEG (*jpg;*.jpeg)|*.jpg;*.jpeg|" +
                    "Portable Network Graphic (*.png)|*.png";
                if (open.ShowDialog() == true)
                {
                    if (!string.IsNullOrEmpty(open.FileName) && File.Exists(open.FileName))
                    {
                        image = new BitmapImage(new Uri(open.FileName));
                        imgPreview.Source = image;
                    }
                    else
                    {
                        MessageBox.Show("Soethig heppened with you file");
                    }
                }
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public byte[] getJPGFromImageControl(BitmapImage imageC)
        {
            MemoryStream memStream = null;
            try
            {
                memStream = new MemoryStream();
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(imageC));
                encoder.Save(memStream);
                
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return memStream.ToArray();
        }

        public BitmapImage ToImage(byte[] array)
        {
            using (var ms = new System.IO.MemoryStream(array))
            {
                var image = new BitmapImage();
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.StreamSource = ms;
                image.EndInit();
                return image;
            }
        }
        
    }
}
