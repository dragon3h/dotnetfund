﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3Products
{
    public class Product
    {
        [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
        public sealed class DecimalPrecisionAttribute : Attribute
        {
            public DecimalPrecisionAttribute(byte precision, byte scale)
            {
                Precision = precision;
                Scale = scale;
            }

            public byte Precision { get; set; }
            public byte Scale { get; set; }
        }

        public Product()
        {
        }

        public Product(string name, decimal price, byte[] photo)
        {
            Name = name;
            Price = price;
            Photo = photo;
        }

        [Key]
        public int ID { get; set; }

        [Column("Name", TypeName = "nvarchar")]
        [MaxLength(50)]
        [Required]
        public string Name { get; set; }

        [DecimalPrecision(8, 2)]
        public decimal Price { get; set; }

        public byte[] Photo { get; set; }
    }
}
