﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tic_Tac_Toe_RR
{
    enum CellState { Empty, X, O }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Button[,] buttonBoard = new Button[3, 3];
        CellState[,] board = new CellState[3, 3];
        int turnCount;

        private void resetGame()
        {
            turnCount = 0;
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    board[row, col] = CellState.Empty;
                    buttonBoard[row, col].Content = "";
                }
            }
        }
        public MainWindow()
        {
            InitializeComponent();
            //buttonBoard[0, 0] = bt00;
            //buttonBoard[0, 1] = bt01;
            //buttonBoard[0, 2] = bt02;
            //buttonBoard[1, 0] = bt10;
            //buttonBoard[1, 1] = bt11;
            //buttonBoard[1, 2] = bt12;
            //buttonBoard[2, 0] = bt20;
            //buttonBoard[2, 1] = bt21;
            //buttonBoard[2, 2] = bt22;
            resetGame();
        }

        private bool isPlayerWinner(CellState player)
        {
            for (int row = 0; row < 3; row++)
            {

                if (board[row, 0] == player && board[row, 1] == player && board[row, 2] == player)
                {
                    return true;
                }
            }
            for (int col = 0; col < 3; col++)
            {
                if (board[0, col] == player && board[1, col] == player && board[2, col] == player)
                {
                    return true;
                }
            }

            if (board[0, 0] == player && board[1, 1] == player && board[2, 2] == player)
            {
                return true;
            }

            if (board[0, 2] == player && board[1, 1] == player && board[2, 0] == player)
            {
                return true;
            }
            return false;
        }

        private void bt_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button) sender;
            string btStr = button.Name;
            int row = int.Parse(btStr.Substring(2, 1));
            int col = int.Parse(btStr.Substring(3));

            if (board[row, col] != CellState.Empty)
            {
                return;
            }
            turnCount++;
            // show the move in the View
            CellState playerSign = turnCount % 2 == 1 ? CellState.X : CellState.O;
            buttonBoard[row, col].Content = playerSign;
            // register the move in Model
            CellState playerCellState = turnCount % 2 == 1 ? CellState.X : CellState.O;
            board[row, col] = playerCellState;
            // is there a winner or do we have a draw?
            if (isPlayerWinner(playerCellState))
            {
                MessageBox.Show(playerCellState + " WON!!!");
                resetGame();
            }
            if (turnCount == 9)
            {
                MessageBox.Show("DRAW!");
                resetGame();
            }
        }
    }
}
