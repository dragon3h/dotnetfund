﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3Runners
{
    //class Runner
    //{
    //    string name;
    //    public string Name
    //    {
    //        get
    //        {
    //            return name;
    //        }
    //    }
    //    List<double> results;
    //    public List<double> Results
    //    {
    //        get
    //        {
    //            return results;
    //        }
    //    }
    //    public Runner(string name, List<double> results)
    //    {
    //        this.name = name;
    //        this.results = results;
    //    }

    //}
    class Program
    {
        static List<double> listOfRunner = null;
        static Dictionary<string, List<double>> listOfResults = new Dictionary<string, List<double>>();
         
        static void Main(string[] args)
        {
            try
            {
                string[] linesArray = File.ReadAllLines("results.txt");
                foreach (string line in linesArray)
                {
                    string[] fieldArray = line.Split(' ');
                    string name = fieldArray[0];
                    double countTime = double.Parse(fieldArray[1]);

                    listOfRunner = new List<double>();
                    if (listOfResults.ContainsKey(name))
                    {
                        listOfRunner = listOfResults[name];
                        listOfRunner.Add(countTime);
                        listOfResults[name] = listOfRunner;
                    }
                    else
                    {
                        listOfRunner.Add(countTime);
                        listOfResults.Add(name, listOfRunner);
                    }
                    
                }
                Console.WriteLine();

                double minTme;
                foreach (string name in listOfResults.Keys)
                {
                    Console.WriteLine("Key = {0}, Results = ",
                            name);
                    listOfRunner = listOfResults[name];
                    minTme = listOfRunner.Min();
                    foreach (double i in listOfRunner)
                    {
                        Console.Write("" + i + " ");
                    }
                    Console.WriteLine("Minimum" + minTme + "\n");
                }

            }
            catch (IOException e)
            {
                Console.WriteLine("Error accessing file.");
                Console.WriteLine(e.StackTrace);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception happened: {0}", e.Message);
                Console.WriteLine(e.StackTrace);
            }
            Console.ReadKey();
        }
    }
}
