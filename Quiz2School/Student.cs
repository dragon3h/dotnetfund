﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2School
{
    class Student
    {
        public Student(int id, string name, int age, double gpa)
        {
            ID = id;
            Name = name;
            Age = age;
            GPA = gpa;
        }
        public int ID { set; get; }
        private string name;
        public string Name 
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                if (value.Length > 1)
                {
                    name = value;
                }
                else
                {
                    throw new ArgumentException("Name has to have minimum 2 characters.");
                }

            }
        }
        private int age;
        public int Age 
        {
            get
            {
                return age;
            }
            set
            {
                if (value >= 1 && value <= 150)
                {
                    age = value;
                }
                else
                {
                    throw new ArgumentException("Age has to be between 1 and 150");
                }
            }
        }
        private double gpa;
        public double GPA 
        {
            get
            {
                return gpa;
            }
            set
            {
                if (value >= 0 && value < 4.3)
                {
                    gpa = value;
                }
                else
                {
                    throw new ArgumentException("gpa has to be between 0 and 4.3 (inclusive)");
                }
            }
        }
    }
}
