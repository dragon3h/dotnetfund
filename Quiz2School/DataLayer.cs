﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2School
{
    class DataLayer
    {

        public DataLayer()
        {
            Connection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=E:\Roman_Romanov\DotNet\Code_C_sharp\Quiz2School\SchoolDb.mdf;Integrated Security=True;Connect Timeout=30");
            Connection.Open();
        }
        private SqlConnection Connection;

        public void addStudent(Student s)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                string name = s.Name;
                int age = s.Age;
                double gpa = s.GPA;

                cmd.CommandText = "INSERT INTO Students (name, age, gpa) VALUES (@Name, @Age, @GPA)";
                cmd.Parameters.AddWithValue("@Name", name);
                cmd.Parameters.AddWithValue("@Age", age);
                cmd.Parameters.AddWithValue("@GPA", gpa);

                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = Connection;
                cmd.ExecuteNonQuery();
            }
        }

        public List<Student> getAllStudents() 
        {
            List<Student> resultList = new List<Student>();
            using (SqlCommand cmd = new SqlCommand("SELECT ID, Name, Age, GPA FROM Students", Connection))
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int ID = reader.GetInt32(0);
                        string name = reader.GetString(1);
                        int age = reader.GetInt32(2);
                        double gpa = reader.GetDouble(3);
                        resultList.Add(new Student(ID, name, age, gpa));
                    }
                }
            }
            return resultList;
        }

        public Student findHighestGPAStudent()
        {
            Student bestStudent = null;
            using (SqlCommand cmd = new SqlCommand("SELECT * FROM Students WHERE GPA = (SELECT MAX(GPA) FROM Students)", Connection))
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int ID = reader.GetInt32(0);
                        string name = reader.GetString(1);
                        int age = reader.GetInt32(2);
                        double gpa = reader.GetDouble(3);
                        bestStudent = new Student(ID, name, age, gpa);
                    }
                }
            }
            return bestStudent;
        }
    }
}
