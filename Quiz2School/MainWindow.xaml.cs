﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz2School
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DataLayer dl;
        public MainWindow()
        {
            dl = new DataLayer();
            InitializeComponent();
            grdStudents.ItemsSource = dl.getAllStudents();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adp.Fill(ds, "LoadDataBinding");
                grdStudents.DataContext = ds;
        }

            grdStudents.ItemsSource = dl.getAllStudents();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            int ID = 1;
            string name = tbName.Text;
            int age = Int32.Parse(tbAge.Text);
            double gpa = Double.Parse(tbGPA.Text);
            Student std = new Student(ID, name, age, gpa);

            dl.addStudent(std);

            grdStudents.ItemsSource = dl.getAllStudents();
        }

        private void btFindHighestGPA_Click(object sender, RoutedEventArgs e)
        {
            string std = dl.findHighestGPAStudent().Name + " " + dl.findHighestGPAStudent().Age;
            MessageBox.Show(String.Format("The best student is: {0}", std));
        }
    }
}
