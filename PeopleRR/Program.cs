﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeapleRR
{
    class Person : IComparable<Person>
    {
        string name;
        public string Name
        {
            get { return name; }
        }
        int age;
        public int Age
        {
            get { return age; }
        }
        public Person(string name, int age)
        {
            this.name = name;
            this.age = age;
        }

        // override CompareTo for Name
        public int CompareTo(Person p)
        {
            return this.Name.CompareTo(p.Name);
        }
    }

    class Student : Person
    {
        double GPA;
        public double gpa
        {
            get { return GPA; }
        }

        public Student(string name, int age, double gpa) : base(name, age)
        {
            this.GPA = gpa;
        }

    }

    class Teacher : Person
    {
        int experienceYears;
        public int ExperienceYears
        {
            get { return experienceYears; }
        }
        public Teacher(String name, int age, int experienceYears)
            : base(name, age)
        {
            this.experienceYears = experienceYears;
        }
    }

    // Create class for compare by Age
    class PersonComparatorByAge : Comparer<Person>
    {
        public override int Compare(Person person1, Person person2)
        {
            if (person1.Age.CompareTo(person2.Age) != 0)
            {
                return person2.Age.CompareTo(person1.Age);
            } 
            else 
                return 0;
        }
    }

    class Program
    {
        readonly static string INPUT_FILE = "input.txt";
        static List<Person> personList = new List<Person>();
        static Dictionary<string, Person> personByName = new Dictionary<string, Person>();


        static void readData() 
        {

            try
            {
                string[] linesOfData = File.ReadAllLines(INPUT_FILE);

                foreach (string line in linesOfData)
                {
                    string[] fieldArray = line.Split(';');

                    if (fieldArray[0] == "Person")
                    {
                        string name = fieldArray[1];
                        int age;
                        int.TryParse(fieldArray[2], out age);
                        Person person = new Person(name, age);
                        personList.Add(person);
                    }

                    if (fieldArray[0] == "Student")
                    {
                        string name = fieldArray[1];
                        int age;
                        int.TryParse(fieldArray[2], out age);
                        double gpa;
                        double.TryParse(fieldArray[3], out gpa);
                        Student student = new Student(name, age, gpa);
                        personList.Add(student);
                    }

                    if (fieldArray[0] == "Teacher")
                    {
                        string name = fieldArray[1];
                        int age;
                        int.TryParse(fieldArray[2], out age);
                        int experienceYears;
                        int.TryParse(fieldArray[3], out experienceYears);
                        Teacher teacher = new Teacher(name, age, experienceYears);
                        personList.Add(teacher);
                    }
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("Error accessing file.");
                Console.WriteLine(e.StackTrace);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception happened: {0}", e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }

        static void Main(string[] args)
        {
            readData();

            // populate data to Dictionary
            foreach (Person p in personList)
            {
                personByName.Add(p.Name, p);
            }

            // offer user enter the name
            Console.WriteLine("Enter the name.");
            string name = Console.ReadLine();

            // find the entered name from Dictionary
            Console.WriteLine("You are looking for: " + personByName[name].Name 
                + " " + personByName[name].Age + " years old.");

            Console.WriteLine();

            SortedDictionary<int, Person> personByAge = new SortedDictionary<int, Person>();

            

            // populate data to SortedDictionary
            foreach (Person p in personList)
            {
                personByAge.Add(p.Age, p);
            }

            // find last element in SortedDictionary
            var lastkey = personByAge.Keys.Last();
            Console.WriteLine("The oldest person is: {0}", personByAge[lastkey].Name);

            Console.WriteLine();

            foreach (KeyValuePair<int, Person> p in personByAge)
            {
                Console.WriteLine("Key = {0}, Name = {1}",
                    p.Key, p.Value.Name);
            }

            Console.WriteLine();

            var listOfAge = personByAge.Keys.ToList();
            listOfAge.Sort();
            
            Console.WriteLine();
            // Loop through keys.
            foreach (var key in listOfAge)
            {
                Console.WriteLine("{0}: {1}", key, personByAge[key]);
            }
            
            Console.WriteLine();

            personList.Sort(); // Sort by defoult by Name
            foreach (Person p in personList)
            {
                Console.WriteLine(p.Name + p.Age);
            }

            Console.WriteLine();

            // Sort uses Comparer<Person> by Age
            personList.Sort(new PersonComparatorByAge());
            foreach (Person p in personList)
            {
                Console.WriteLine(p.Name + p.Age);
            }

            Console.WriteLine();


            foreach (Person p in personList)
            {
                if (p.GetType() == typeof(Student))
                {
                    Student s = (Student)Convert.ChangeType(p, typeof(Student));
                    Console.WriteLine("Name is: " + s.Name + " age is " + s.Age + " GPA: " + s.gpa);
                }
                else
                    Console.WriteLine(p.Name + " " + p.Age);
            }
            Console.ReadKey();
        }
    }
}
