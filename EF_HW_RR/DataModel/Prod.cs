﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_HW_RR.DataModel
{
    enum typesOfProduct { toys, garden, other }
    public class Prod
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public byte[] Photo { get; set; }
    }
}
