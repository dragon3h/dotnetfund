﻿using EF_HW_RR.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_HW_RR
{
    class Program
    {
        

        static void Main(string[] args)
        {
            using (Product prd = new Product())
            {
                Prod product = new Prod();
                product.Name = "toy";
                product.Price = 10.50m;

                prd.Products.Add(product);
                prd.SaveChanges();
            }

        }
    }
}
