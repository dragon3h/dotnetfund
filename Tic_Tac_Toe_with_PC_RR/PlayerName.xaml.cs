﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TicTacToe
{
    /// <summary>
    /// Interaction logic for PlayerName.xaml
    /// </summary>
    public partial class PlayerName : Window
    {
        //string player1Name;
        string player2Name;
        string player1Name;
        public PlayerName()
        {
            InitializeComponent();
        }

        private void btOk_Click(object sender, RoutedEventArgs e)
        {
            player1Name = tbPlayer1.Text;
            player2Name = tbPlayer2.Text;

            MainWindow mw = new MainWindow(player1Name, player2Name);
            if (mw.ShowDialog() == true)
            {

            }
            Close();
        }
    }
}
