﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TicTacToe
{

    enum CellState { Empty, X, O }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Button[,] buttonBoard = new Button[3, 3];
        CellState[,] board = new CellState[3, 3];
        int turnCount;

        private void resetGame()
        {
            turnCount = 0;
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    board[row, col] = CellState.Empty;
                    buttonBoard[row, col].Content = "";
                }
            }
        }

        private void HandleClick(int row, int col)
        {
            // WRONG: if (buttonBoard[row,col].Content != "") { }
            // CORRECT APPROACH BELOW:
            if (board[row, col] != CellState.Empty)
            {
                return;
            }
            turnCount++;
            // show the move in the View
            CellState playerSign = turnCount % 2 == 1 ? CellState.X : CellState.O;
            buttonBoard[row, col].Content = playerSign;
            // register the move in Model
            CellState playerCellState = turnCount % 2 == 1 ? CellState.X : CellState.O;
            board[row, col] = playerCellState;
            // is there a winner or do we have a draw?
            if (isPlayerWinner(playerCellState))
            {
                MessageBox.Show(playerCellState + " WON!!!");
                resetGame();
            }
            if (turnCount == 9)
            {
                MessageBox.Show("DRAW!");
                resetGame();
            }

        }

        private bool isPlayerWinner(CellState player)
        {
            Boolean equal = true;
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    if (board[row, col] != player)
                    {
                        equal = false;
                        break;
                    }
                }
                if (equal)
                {
                    return true;
                }
            }

            //        if (board[row, 0] == player && board[row, 1] == player && board[row, 2] == player)
            //        {
            //            return true;
            //        }
            //}
            for (int col = 0; col < 3; col++)
            {
                if (board[0, col] == player && board[1, col] == player && board[2, col] == player)
                {
                    return true;
                }
            }

            if (board[0, 0] == player && board[1, 1] == player && board[2, 2] == player)
            {
                return true;
            }

            if (board[0, 2] == player && board[1, 1] == player && board[2, 0] == player)
            {
                return true;
            }
            return false;
        }

        public MainWindow()
        {
            InitializeComponent();
            buttonBoard[0, 0] = Bt00;
            buttonBoard[0, 1] = Bt01;
            buttonBoard[0, 2] = Bt02;
            buttonBoard[1, 0] = Bt10;
            buttonBoard[1, 1] = Bt11;
            buttonBoard[1, 2] = Bt12;
            buttonBoard[2, 0] = Bt20;
            buttonBoard[2, 1] = Bt21;
            buttonBoard[2, 2] = Bt22;
            resetGame();
        }

        public MainWindow(string nameX, string nameO)
        {
            InitializeComponent();
            lblPlayerX.Content = nameX;
            lblPlayerO.Content = nameO;
        }


        private void Bt00_Click(object sender, RoutedEventArgs e)
        {
            HandleClick(0, 0);

        }

        private void Bt01_Click(object sender, RoutedEventArgs e)
        {
            HandleClick(0, 1);

        }

        private void Bt02_Click(object sender, RoutedEventArgs e)
        {
            HandleClick(0, 2);

        }

        private void Bt10_Click(object sender, RoutedEventArgs e)
        {
            HandleClick(1, 0);
        }

        private void Bt11_Click(object sender, RoutedEventArgs e)
        {
            HandleClick(1, 1);
        }

        private void Bt12_Click(object sender, RoutedEventArgs e)
        {
            HandleClick(1, 2);
        }

        private void Bt20_Click(object sender, RoutedEventArgs e)
        {
            HandleClick(2, 0);
        }

        private void Bt21_Click(object sender, RoutedEventArgs e)
        {
            HandleClick(2, 1);
        }

        private void Bt22_Click(object sender, RoutedEventArgs e)
        {
            HandleClick(2, 2);

        }

        private void btPC_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btHuman_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("Enter name of the first player");
            PlayerName pn = new PlayerName();
            pn.Owner = this;
            pn.ShowDialog();
        }

    }
}
