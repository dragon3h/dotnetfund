﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingLotRR
{
    class Car
    {
        private string _Make;
        public string Make
        {
            get
            {
                return _Make;
            }
            set
            {
                if (value.Length > 1)
                {
                    _Make = value;
                }
            }
        }
        string _Model;
        public string Model
        {
            get
            {
                return _Model;
            }
            set
            {
                if (value.Length > 1)
                {
                    _Model = value;
                }
            }
        }
        private int _YOP;
        public int YOP
        {
            get
            {
                return _YOP;
            }
            set
            {
                if (value > 1900 || value <= 2100)
                {
                    _YOP = value;
                }
            }
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            List<Car> carList = new List<Car>();

            while (true)
            {
                Car car = new Car();
                Console.WriteLine("Enter the car's make (empty for Exit)");
                string make = Console.ReadLine();
                if (make == "") { break; }
                car.Make = make; 
                Console.WriteLine("Enter the cars's model (empty for Exit)");
                string model = Console.ReadLine();
                if (model == "") { break; }
                car.Model = model;
                Console.WriteLine("Enter the cars's year production (empty for Exit)");
                string yopStr = Console.ReadLine();
                if (yopStr == "") { break; }
                int yop;
                if (int.TryParse(yopStr, out yop))
                {
                    car.YOP = yop;
                }

                carList.Add(car);
            }

            foreach (Car car in carList)
            {
                Console.WriteLine(car.Make + " " + car.Model + " " + car.YOP);
            }
            Console.ReadKey();
        }
    }
}
