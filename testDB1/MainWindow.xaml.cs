﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace testDB1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Person> listOfPerson = new List<Person>();
        static SqlConnection Connection = new SqlConnection(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename =| DataDirectory |\PeopleDB.mdf; Integrated Security = True; Connect Timeout = 30");
        Database db = new Database(Connection);
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                string name = tbName.Text;
                int age = Int32.Parse(tbAge.Text);

                cmd.CommandText = "INSERT INTO Person (name, age) VALUES (@Name, @Age)";
                cmd.Parameters.AddWithValue("@Name", name);
                cmd.Parameters.AddWithValue("@Age", age);

                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = Connection;
                cmd.ExecuteNonQuery();
            }
        }
    }
}
