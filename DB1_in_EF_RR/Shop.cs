namespace DB1_in_EF_RR
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    public class Shop : DbContext
    {

        // Your context has been configured to use a 'Shop' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'DB1_in_EF_RR.Shop' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'Shop' 
        // connection string in the application configuration file.

        public Shop()
            : base("name=Shop")
        {
        }

        public DbSet<Product> Products { get; set; }

        public void addProduct(Product product)
        {
            using (Shop context = new Shop())
            {
                context.Products.Add(product);
                context.SaveChanges();
            }
        }

        public void updateProductById(Product p, int id)
        {
            using (Shop ctx = new Shop())
            {
                try
                {
                    Product prd = ctx.Products.First(i => i.ID == id);
                    prd.Name = p.Name;
                    prd.Price = p.Price;
                    prd.Photo = p.Photo;

                    ctx.SaveChanges();
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (InvalidOperationException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }  
        }

        public List<Product> getAllProducts()
        {
            List<Product> resultList = new List<Product>();

            using (Shop context = new Shop())
            {
                var rows = (from p in context.Products select p);
                foreach (Product prd in rows)
                {
                    resultList.Add(prd);
                }
 
            }
            return resultList;
        }

        public void deleteItemBy(int id)
        {
            using (Shop ctx = new Shop())
            {
                try
                {
                    var prd = new Product { ID = id };
                    ctx.Products.Attach(prd);
                    ctx.Products.Remove(prd);
                    ctx.SaveChanges();
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (InvalidOperationException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}