﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DB1_in_EF_RR
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BitmapImage image;
        int id = 0;
        enum typesOfProduct { toys, garden, other }
        public MainWindow()
        {
            InitializeComponent();
            fillComboboxesTime();
            using (var ctx = new Shop())
            {
                dgrdProduct.ItemsSource = ctx.getAllProducts();
            }
        }

        public void fillComboboxesTime()
        {
            foreach (var value in Enum.GetValues(typeof(typesOfProduct)))
            {
                string typeOfProduct = value.ToString();
                cmbTypeOfProduct.Items.Add(typeOfProduct);
            }
            cmbTypeOfProduct.SelectedItem = typesOfProduct.other.ToString();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            string type = cmbTypeOfProduct.Text;
            decimal price = decimal.Parse(tbPrice.Text);
            byte[] photo;
            if (!(image == null))
            {
                photo = getJPGFromImageControl(image as BitmapImage);
            }
            else
            {
                photo = null;
            }

            Product prd = new Product(name, type, price, photo);

            using (var ctx = new Shop())
            {
                ctx.addProduct(prd);
                dgrdProduct.ItemsSource = ctx.getAllProducts();
                dgrdProduct.Items.Refresh();
            }
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            using (var ctx = new Shop())
            {
                ctx.deleteItemBy(id);
                dgrdProduct.ItemsSource = ctx.getAllProducts();
                dgrdProduct.Items.Refresh();
            }             
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            string type = cmbTypeOfProduct.Text;
            decimal price = decimal.Parse(tbPrice.Text);
            byte[] photo;
            if (!(image == null))
            {
                photo = getJPGFromImageControl(image as BitmapImage);
            }
            else
            {
                photo = null;
            }

            Product prd = new Product(name, type, price, photo);

            using (var ctx = new Shop())
            {
                ctx.updateProductById(prd, id);
                dgrdProduct.ItemsSource = ctx.getAllProducts();
                dgrdProduct.Items.Refresh();
            }
        }

        private void btImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Title = "Select a picture";
            open.Filter = "All supported grahhics|*.jpg;*.jpeg;*.png|" +
                "JPEG (*jpg;*.jpeg)|*.jpg;*.jpeg|" +
                "Portable Network Graphic (*.png)|*.png";
            if (open.ShowDialog() == true)
            {
                if (!string.IsNullOrEmpty(open.FileName) && File.Exists(open.FileName))
                {
                    image = new BitmapImage(new Uri(open.FileName));
                }
                else
                {
                    MessageBox.Show("Soethig heppened with you file");
                }
            }
        }

        public byte[] getJPGFromImageControl(BitmapImage imageC)
        {
            MemoryStream memStream = new MemoryStream();
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(imageC));
            encoder.Save(memStream);
            return memStream.ToArray();
        }

        private void dgrdProduct_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            Product prd = (Product)dgrdProduct.SelectedItem;
            if (prd == null)
            {
                return;
            }
            id = prd.ID;
            lblID.Content = id;
            tbName.Text = prd.Name;
            tbPrice.Text = prd.Price.ToString();
            cmbTypeOfProduct.SelectedValue = prd.TypeOfProduct;
        }
    }
}
