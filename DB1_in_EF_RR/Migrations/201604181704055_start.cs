namespace DB1_in_EF_RR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class start : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "TypeOfProduct", c => c.String(nullable: false, maxLength: 20));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "TypeOfProduct");
        }
    }
}
