﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CarsDatabase_RR
{
    class Car
    {
        public Car(int? id, string plate, string make, string model, int? yop)
        {
            ID = id;
            Plate = plate;
            Make = make;
            Model = model;
            YOP = yop;
        }

        public int? ID { set; get; }
        public string Plate { set; get; }
        public string Make { set; get; }
        public string Model { set; get; }
        public int? YOP { set; get; }
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<Car> CarsList = new ObservableCollection<Car>();
        string filePath = null;
        public MainWindow()
        {
            //CarsList.Add(new Car(1, "HE45YI", "Toyta", "Corolla", 2006));
            //CarsList.Add(new Car(2, "JR12PO", "BMW", "525", 2014));
            //CarsList.Add(new Car(3, "MN2WSD", "Audi", "A6", 2015));
            InitializeComponent();
            //lvCarsList.ItemsSource = CarsList;
        }

        private void lvCarsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvCarsList.SelectedIndex != -1)
            {
                int index = lvCarsList.SelectedIndex;
                showCar(CarsList[index]);
            }
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            if (CarsList.Count < 1)
            {
                int id0 = 1;
                string plate0 = tbPlate.Text;
                string make0 = tbMake.Text;
                string model0 = tbModel.Text;
                int year0 = 0;
                Int32.TryParse(tbYOP.Text, out year0);
                CarsList.Add(new Car(id0, plate0, make0, model0, year0));
                lvCarsList.ItemsSource = CarsList;
            }
            else
            {
                var maxID = CarsList.Max(car => car.ID);
                int? id = maxID + 1;
                string plate = tbPlate.Text;
                string make = tbMake.Text;
                string model = tbModel.Text;
                int year = 0;
                Int32.TryParse(tbYOP.Text, out year);
                Car c = new Car(id, plate, make, model, year);
                CarsList.Add(c);
            }

            showCar(new Car(null, "", "", "", null));
            //lvCarsList.Items.Refresh();
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            int index = lvCarsList.SelectedIndex;
            CarsList[index].Plate = tbPlate.Text;
            CarsList[index].Make = tbMake.Text;
            CarsList[index].Model = tbModel.Text;
            int year = 0;
            Int32.TryParse(tbYOP.Text, out year);
            CarsList[index].YOP = year;

            lvCarsList.Items.Refresh();
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lvCarsList.SelectedIndex != -1)
                {
                    Car car = (Car)lvCarsList.SelectedItem;
                    var itemToRemove = CarsList.Single(r => r.ID == car.ID);
                    CarsList.Remove(itemToRemove);
                    showCar(new Car( null,"","","", null));
                }
            }
            catch (ArgumentNullException ex)
            {
                MessageBox.Show("ArgumentNullException occured.");
                MessageBox.Show(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show("InvalidOperationException occured.");
                MessageBox.Show(ex.Message);
            }
        }

        private void OpenClick(object sender, RoutedEventArgs e)
        {
            CarsList.Clear();
            showCar(new Car(null, "", "", "", null));

            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "Car's list (*.car)|*.car*|All files (*.*)|*.*";
            openFile.FilterIndex = 1;
            openFile.Multiselect = false;
            
            try
            {
                if (openFile.ShowDialog() == true)
                {
                    filePath = openFile.FileName;
                    string[] lines = File.ReadAllLines(filePath);
                    foreach (string line in lines)
                    {

                        string[] carData = line.Split('|');
                        int id = 0;
                        Int32.TryParse(carData[0], out id);
                        string plate = carData[1];
                        string make = carData[2];
                        string model = carData[3];
                        int year = 0;
                        Int32.TryParse(carData[4], out year);

                        Car car = new Car(id, plate, make, model, year);
                        CarsList.Add(car);
                    }
                    lvCarsList.ItemsSource = CarsList;
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("Read file exception occured!");
                MessageBox.Show(ex.Message);
            }
            
        }

        private void SaveAsClick(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.Filter = "Car's list (*.car)|*.car*|All files (*.*)|*.*";

            if (saveFile.ShowDialog() == true)
            {
                string[] listOfCars = new string[CarsList.Count];
                int ind = 0;
                foreach (Car car in CarsList)
                {
                    string id = car.ID.ToString();
                    string plate = car.Plate;
                    string make = car.Make;
                    string model = car.Model;
                    string yearCar = car.YOP.ToString();
                    string carLine = id + "|" + plate + "|" + make + "|" + model + "|" + yearCar;
                    listOfCars[ind] = carLine;
                    ind++;
                }
                File.WriteAllLines(saveFile.FileName, listOfCars);

            }
        }

        private void ExitClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void showCar(Car car)
        {
            lblID.Content = car.ID;
            tbMake.Text = car.Make;
            tbModel.Text = car.Model;
            tbPlate.Text = car.Plate;
            string yearCar = car.YOP.ToString();
            tbYOP.Text = yearCar;
        }
    }
}
