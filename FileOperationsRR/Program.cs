﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileOperationsRR
{
    class Program
    {
        static void Main(string[] args)
        {
            
            try
            {
                Console.Write("What is your name? : ");
                string name = Console.ReadLine();
                Console.WriteLine();
                Console.Write("Enter file name: ");
                string fileName = Console.ReadLine();
                Console.WriteLine();

                string addStr = string.Format("Nice to meet you, {0}!", name);

                File.WriteAllText(fileName, addStr);

                //StreamWriter writer = new StreamWriter(fileName);
                //{
                //    writer.Write(addStr);
                //    writer.Close();
                //}

                string text = File.ReadAllText(fileName);
                Console.WriteLine("The line I found in the file was: {0}", text);

                //using (StreamReader sr = new StreamReader(fileName))
                //{
                //    // Read the stream to a string, and write the string to the console.
                //    String line = sr.ReadToEnd();
                //    Console.WriteLine("The line I found in the file was: {0}", line);
                //}
            }
            catch (IOException e)
            {
                Console.Write(e.StackTrace);
                Console.WriteLine(e.Message);
            }

            Console.ReadKey();
        }
    }
}
