﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz4WordPlay
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static Dictionary<string, int> result = new Dictionary<string, int>();
        static Dictionary<string, int> textInput = new Dictionary<string, int>();
        string[] output = new string[2] { "", "" };
 
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btRead_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                
                string str = @"..\..\" + tboxInput.Text;
                string[] linesArray = File.ReadAllLines(str);
                string textIn = "";

                foreach (string line in linesArray)
                {
                    textIn +=line + " ";
                }

                string[] splitwordsInput = System.Text.RegularExpressions.Regex.Split(textIn, "[^\\w]");

                foreach (string w in linesArray)
                {
                    lblInput.Content += w + string.Format("\n");
                }

                foreach (string word in splitwordsInput)
                {
                    if (word == "")
                    {
                        continue;
                    }
                    if (!textInput.ContainsKey(word))
                    {
                        textInput.Add(word, 0);
                    }
                    textInput[word]++;
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error reading file\n{0}", ex.StackTrace);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error reading file\n{0}", ex.StackTrace);
            }

        }

        private void btWrite_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                
                string str = @"..\..\" + tboxOutput.Text;
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(str))
                {
                    //string line = lblOutput.ToString();
                    foreach (string line in output)
                    {
                        file.WriteLine(line);
                    }
                    
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error writing file\n{0}", ex.StackTrace);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error writing file\n{0}", ex.StackTrace);
            }
        }

        private void btAnalyze_Click(object sender, RoutedEventArgs e)
        {
            string wordsToFind = tboxWordsList.Text;
            string[] splitwordsToFind = System.Text.RegularExpressions.Regex.Split(wordsToFind, "[^\\w]");

            foreach (string w in splitwordsToFind)
            {
                if (!textInput.ContainsKey(w))
                {
                    continue;
                }
                if (!result.ContainsKey(w))
                {
                    result.Add(w, textInput[w]);
                }
            }

            foreach (string w in result.Keys)
            {
                lblOutput.Content += w + " : " + result[w] + "\n";
            }

            var sortedResult = from w in result
                               orderby w.Value descending,
                                       w.Key
                               select w;

            //lblOutput.Content = "";
            int max = 0;
            string keyMax = "";
            int stop = 0;

            while (result.Count > 0 && stop < 2)
            {
                max = 0;
                keyMax = "";
                foreach (string w in result.Keys)
                {
                    if (max < result[w])
                    {
                        max = result[w];
                        keyMax = w;
                    }
                }
                lblOutput.Content += string.Format(keyMax + " : {0}\n", result[keyMax]);
                output[stop] = keyMax + " : " + result[keyMax];
                result.Remove(keyMax);
                stop++;
            }
        }
    }
}
