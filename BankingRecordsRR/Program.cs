﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankingRecords
{
    public class AccountTransaction
    {
        private DateTime _date;
        private string _description;
        private decimal _deposit;
        private decimal _withdrawal;

        public AccountTransaction(string lineOfData)
        {
            string[] fieldArray = lineOfData.Split(';');
            // method 1: this may throw FormatException - we let it be thrown
            _date = DateTime.Parse(fieldArray[0]);
            // method 2: we throw our own exception
            _description = fieldArray[1];
            if (_description.Length < 2)
            {
                throw new ArgumentException(
                    string.Format("Invalid field value '{0}', " +
                    "description must be at least 2 characters long in {1}",
                    _description, lineOfData)
                    );
            }
            // method 3: we check a condition and throw our own exception again
            if (!decimal.TryParse(fieldArray[2], out _deposit))
            {
                throw new ArgumentException(
                    string.Format("Invalid field value '{0}', " +
                    "must be a decimal in {1}",
                    fieldArray[2], lineOfData)
                    );
            }
            // method 4: let exception be thrown, catch and re-throw it as chained exception
            try
            {
                _withdrawal = decimal.Parse(fieldArray[3]);
            }
            catch (FormatException e)
            {
                throw new ArgumentException(
                    string.Format("Invalid field value '{0}', " +
                    "must be a decimal in {1}",
                    fieldArray[3], lineOfData), e // e: original exception
                    );
            }
            // FIXME: either deposit or withdrawal MUST BE 0
            if (_deposit < 0)
            {
                throw new ArgumentException("Deposit must not be negative");
            }
            if (_withdrawal < 0)
            {
                throw new ArgumentException("Withdrawal must not be negative");
            }
            if (!(_withdrawal == 0 ^ _deposit == 0))
            {
                throw new ArgumentException("You can provide only one operation in the same time");
            }
            // phylosophical debate: OR vs XOR
        }
        public DateTime Date
        {
            get { return _date; }
        }
        public string Description
        {
            get { return _description; }
        }
        public decimal Deposit
        {
            get { return _deposit; }
        }
        public decimal Withdrawal
        {
            get { return _withdrawal; }
        }
    }


    class Program
    {
        static List<AccountTransaction> transactionList = new List<AccountTransaction>();
        static void Main(string[] args)
        {
            decimal finalBalance = 0;
            decimal totalDeposit = 0;
            decimal totalWithdrawl = 0;
            try
            {
                string[] linesArray =
                    File.ReadAllLines("operations.txt");
                foreach (string line in linesArray)
                {
                    AccountTransaction at = new AccountTransaction(line);
                    transactionList.Add(at);
                    totalDeposit += at.Deposit;
                    totalWithdrawl += at.Withdrawal;
                }
                finalBalance += (totalDeposit - totalWithdrawl);
                Console.WriteLine("The total deposit is : {0}", totalDeposit);
                Console.WriteLine("The total withdrawl is : {0}", totalWithdrawl);
                Console.WriteLine("The final balance of the account is : {0}", finalBalance);
            }
            catch (IOException e)
            {
                Console.WriteLine("Error accessing file.");
                Console.WriteLine(e.StackTrace);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception happened: {0}", e.Message);
                Console.WriteLine(e.StackTrace);
            }
            Console.WriteLine(transactionList[2].Date);
            Console.ReadKey();
        }
    }
}
